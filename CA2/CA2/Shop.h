#pragma once
#include <string>
#include <list>
#include <fstream>
#include "Tool.h"
#include "Ware.h"
#include "Supply.h"

class Shop
{

private:
	bool isAdmin; // Admin/Shopper mode.
	list<Tool> tools; // List for tools.
	list<Supply> supplies; // List for supplies.
	list<Ware> cart; // Cart for shoppers.
	Tool tempTool;
	ifstream inputS; // Input file stream.
	ofstream outputS; // Output file stream.
	string currentChoice;

public:
	Shop( const bool &isAd ); // Constructor method.
	void load(); // Loads list of items.
	void display(); // Displays main text and options.
	void displayAdmin(); // Displays admin's UI.
	void addItem(); // Adds items into list.(Admin)
	void editItem(); // Edits items from list.(Admin)
	void removeItem(); // Removes item from list.(Admin)
	void saveUpdate(); // Saves list of items.(Admin)
	void displayShopper(); // Displays shopper's UI.
	void showShopperOptions( const int &type ); // Shows items on sale and options.
	void displayTools(); // Display tools from list.
	void displaySupplies(); // Display supplies from list.
};