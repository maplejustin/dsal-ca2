#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <list>
#include "Shop.h"

using namespace std;

// Constructor method.
Shop::Shop( const bool &isAd )
{
	isAdmin = isAd;
	
	//inputS.open( "tools.txt" );
	//inputS.close();
}

// Compare by name.
bool cmp_by_name( const Ware &a, const Ware &b )
{
	return a.getName() < b.getName();
}

// Compare by cost.
bool cmp_by_cost( const Ware &a, const Ware &b )
{
	return a.getCost() < b.getCost();
}

// Compare by rating.
bool cmp_by_rate( const Tool &a, const Tool &b )
{
	return a.getRating() < b.getRating();
}

// Loads list of items.
void Shop::load()
{
	inputS.open( "tools.txt" );
	char test;
	inputS >> test;

	if( !inputS.eof() )
	{
		string name = "";
		string desc = "";
		string fact = "";
		string col = "";

		// Gets data of tools.
		while( test != '#' && test != ' ' )
		{
			
			name = "";
			desc = "";
			fact = "";
			col = "";

			while( test != '#' )
			{
				name += test;
				inputS >> test;
			}

			inputS >> test;

			while( test != '#' )
			{
				desc += test;
				inputS >> test;
			}
	
			double cost;
			inputS >> cost;
			inputS >> test;

			inputS >> test;

			while( test != '#' )
			{
				fact += test;
				inputS >> test;
			}

			inputS >> test;
		
			while( test != '#' )
			{
				col += test;
				inputS >> test;
			}

			int rate;
			inputS >> rate;
			inputS >> test;
			inputS >> test;

			//tempTool = &Tool( name, desc, cost, fact, col, rate );
			tools.push_back( Tool( name, desc, cost, fact, col, rate ) );
			//tools.push_back( &( Tool( name, desc, cost, fact, col, rate ) ) );

		}

	}

	
	inputS.close();

	inputS.open( "supplies.txt" );
	inputS >> test;

	if( !inputS.eof() )
	{
		string name = "";
		string desc = "";
		string fact = "";

		// Gets data of tools.
		while( test != '#' && test != ' ' )
		{
			
			name = "";
			desc = "";
			fact = "";

			while( test != '#' )
			{
				name += test;
				inputS >> test;
			}

			inputS >> test;

			while( test != '#' )
			{
				desc += test;
				inputS >> test;
			}
	
			double cost;
			inputS >> cost;
			inputS >> test;

			inputS >> test;

			while( test != '#' )
			{
				fact += test;
				inputS >> test;
			}

			//inputS >> test;

			int amt;
			inputS >> amt;
			inputS >> test;
			inputS >> test;

			//tempTool = &Tool( name, desc, cost, fact, col, rate );
			supplies.push_back( Supply( name, desc, cost, fact, amt ) );
			//tools.push_back( &( Tool( name, desc, cost, fact, col, rate ) ) );

		}

	}

	
	inputS.close();
}

// Displays main text and options.
void Shop::display()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	load();

	// Pre: parameter isAdmin is true.
	if( isAdmin )
	{
		// Post: display options for admin.
		displayAdmin();
	}
	else
	{
		// Post: display options for shopper.
		displayShopper();
	}
}

// Displays admin's UI.
void Shop::displayAdmin()
{
	cout << "Welcome back to your shop! What would you like to do?" << endl << endl;

	// Print choices.
	for( int i = 0; i < 4; i++ )
	{
		switch( i )
		{
		case 0: cout << "1) Add items" << endl;
			break;
		case 1: cout << "2) Edit items" << endl;
			break;
		case 2: cout << "3) Remove items" << endl;
			break;
		case 3: cout << "4) Save update" << endl;
			break;
		default: cout << "" << endl;
			break;
		}
	}

	cout << "Your choice: ";
	getline( cin, currentChoice );

	// Fire methods for each choice.
	if( currentChoice == "1" || currentChoice == "Add" )
	{
		// Post: add items.
		addItem();
	}
	else if( currentChoice == "2" || currentChoice == "Edit" )
	{
		// Post: edit items.
		editItem();
	}
	else if( currentChoice == "3" || currentChoice == "Remove" )
	{
		// Post: remove items.
		removeItem();
	}
	else if( currentChoice == "4" || currentChoice == "Remove" )
	{
		// Post: save update.
		saveUpdate();
	}
	else
	{
		// Post: re-prompt.
		cout << "Please enter a valid choice! " << endl << endl;
		displayAdmin();
	}
}

// Adds items into list.(Admin)
void Shop::addItem()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

	// Asks if adding tools or supplies.
	cout << endl << "What type of wares would you want to add?" << endl;
	cout << "1) Tool" << endl;
	cout << "2) Supply" << endl;
	cout << "Your choice: ";

	getline( cin, currentChoice );

	// Pre: compare input given.
	if( currentChoice == "1" || currentChoice == "Tool" )
	{
		// Post: add tool.
		string n, desc, fact, col;
		double c;
		int rate;

		cout << endl;
		cout << endl << "Name of tool: ";
		getline( cin, n );
		cout << endl << "Description of tool: ";
		getline( cin, desc );
		cout << endl << "Cost of tool: ";
		cin >> c;
		getline( cin, fact );
		cout << endl << "Factory made: ";
		getline( cin, fact );
		cout << endl << "Color of tool: ";
		getline( cin, col );
		cout << endl << "Rating of tool: ";
		cin >> rate;

		Tool tempTool = Tool( n, desc, c, fact, col, rate );
		tools.push_back( tempTool );

		cout << "Item added!" << endl;
		// Options for admin.
		cin.ignore();
		displayAdmin();
	}
	else if( currentChoice == "2" || currentChoice == "Supply" )
	{
		// Post: add supply.
		string n, desc, fact;
		double c;
		int amt;

		cout << endl;
		cout << endl << "Name of supply: ";
		getline( cin, n );
		cout << endl << "Description of supply: ";
		getline( cin, desc );
		cout << endl << "Cost of supply: ";
		cin >> c;
		getline( cin, fact );
		cout << endl << "Factory made: ";
		getline( cin, fact );
		cout << endl << "Amount of supply: ";
		cin >> amt;

		Supply tempSup = Supply( n, desc, c, fact, amt );
		supplies.push_back( tempSup );

		cout << "Item added!" << endl;
		// Options for admin.
		cin.ignore();
		displayAdmin();
	}
}

// Edits items from list.(Admin)
void Shop::editItem()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

	// Asks if editting tools or supplies.
	cout << endl << "What type of wares would you want to edit?" << endl;
	cout << "1) Tool" << endl;
	cout << "2) Supply" << endl;
	cout << "Your choice: ";

	getline( cin, currentChoice );

	// Pre: compare input given.
	if( currentChoice == "1" || currentChoice == "Tool" )
	{
		// Post: edit tool.

		// Choose tool.
		int c = 0;

		while( c > tools.size() || c <= 0 )
		{
			cout << "Choose tool to edit: " << endl << endl;

			/*list<Tool>::iterator toolIT = tools.begin();
			for( int i = 0; i < tools.size(); i++ )
			{
				Tool tempTool = *toolIT;
				std::ostringstream convert;
				string z;
				convert << i + 1;
				z = convert.str();
				cout << z << ") " << tempTool.getName() << endl;
				toolIT++;
			}*/
			displayTools();

			cout << "Choice: ";

			cin >> c;

			if( c > tools.size() || c <= 0 )
				cout << endl << endl << "Invalid choice!" << endl;
		}

		// Choose property.
		int choice = 0;

		while( choice > 6 || choice <= 0 )
		{
			
			cout << endl << endl << "Choose property to edit: " << endl;
			for( int i = 0; i < 6; i++ )
			{
				switch( i )
				{
				case 0: cout << "1) Name" << endl;
					break;
				case 1: cout << "2) Description" << endl;
					break;
				case 2: cout << "3) Cost" << endl;
					break;
				case 3: cout << "4) Factory" << endl;
					break;
				case 4: cout << "5) Color" << endl;
					break;
				case 5: cout << "6) Rating" << endl;
					break;
				default: cout << "" << endl;
					break;
				}
			}

			cout << endl << "Choice: ";

			cin >> choice;

			if( choice > 6 || choice <= 0 )
				cout << endl << endl << "Invalid choice!" << endl;
		}

		// Get tool of selection.
		list<Tool>::iterator toolIT = tools.begin();
		for( int i = 0; i < c - 1; i++ )
		{
			toolIT++;
		}
		

		// Edit.
		switch( choice - 1 )
		{
		case 0: 
			{
				string edit;
				cout << endl << "New name: ";
				cin.ignore();
				getline( cin, edit );
				( *toolIT ).setName( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 1: 
			{
				string edit;
				cout << endl << endl << "New description: ";
				cin.ignore();
				getline( cin, edit );
				( *toolIT ).setDesc( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 2: 
			{
				double edit;
				cout << endl << endl << "New cost: ";
				cin.ignore();
				cin >> edit;
				( *toolIT ).setCost( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 3: 
			{
				string edit;
				cout << endl << endl << "New factory: ";
				cin.ignore();
				getline( cin, edit );
				( *toolIT ).setFact( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 4: 
			{
				string edit;
				cout << endl << endl << "New color: ";
				cin.ignore();
				getline( cin, edit );
				( *toolIT ).setColor( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 5: 
			{
				int edit;
				cout << endl << endl << "New rating: ";
				cin.ignore();
				cin >> edit;
				( *toolIT ).setRating( edit );
				cout << endl << "Item updated!" << endl;
			}
			break;
		}

		displayAdmin();

	}
	else if( currentChoice == "2" || currentChoice == "Supply" )
	{
		// Post: edit supply.
		
		// Choose supply.
		int c = 0;

		while( c > supplies.size() || c <= 0 )
		{
			cout << "Choose supply to edit: " << endl << endl;

			/*list<Tool>::iterator toolIT = tools.begin();
			for( int i = 0; i < tools.size(); i++ )
			{
				Tool tempTool = *toolIT;
				std::ostringstream convert;
				string z;
				convert << i + 1;
				z = convert.str();
				cout << z << ") " << tempTool.getName() << endl;
				toolIT++;
			}*/
			displaySupplies();

			cout << "Choice: ";

			cin >> c;

			if( c > supplies.size() || c <= 0 )
				cout << endl << endl << "Invalid choice!" << endl;
		}

		// Choose property.
		int choice = 0;

		while( choice > 5 || choice <= 0 )
		{
			
			cout << endl << endl << "Choose property to edit: " << endl;
			for( int i = 0; i < 5; i++ )
			{
				switch( i )
				{
				case 0: cout << "1) Name" << endl;
					break;
				case 1: cout << "2) Description" << endl;
					break;
				case 2: cout << "3) Cost" << endl;
					break;
				case 3: cout << "4) Factory" << endl;
					break;
				case 4: cout << "5) Amount" << endl;
					break;
				default: cout << "" << endl;
					break;
				}
			}

			cout << endl << "Choice: ";

			cin >> choice;

			if( choice > 6 || choice <= 0 )
				cout << endl << endl << "Invalid choice!" << endl;
		}

		// Get supply of selection.
		list<Supply>::iterator supIT = supplies.begin();
		for( int i = 0; i < c - 1; i++ )
		{
			supIT++;
		}
		

		// Edit.
		switch( choice - 1 )
		{
		case 0: 
			{
				string edit;
				cout << endl << "New name: ";
				cin.ignore();
				getline( cin, edit );
				( *supIT ).setName( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 1: 
			{
				string edit;
				cout << endl << endl << "New description: ";
				cin.ignore();
				getline( cin, edit );
				( *supIT ).setDesc( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 2: 
			{
				double edit;
				cout << endl << endl << "New cost: ";
				cin.ignore();
				cin >> edit;
				( *supIT ).setCost( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 3: 
			{
				string edit;
				cout << endl << endl << "New factory: ";
				cin.ignore();
				getline( cin, edit );
				( *supIT ).setFact( edit );
				cout << endl << "Item updated!";
			}
			break;
		case 4: 
			{
				int edit;
				cout << endl << endl << "New amount: ";
				cin.ignore();
				cin >> edit;
				( *supIT ).setAmt( edit );
				cout << endl << "Item updated!" << endl;
			}
			break;
		}

		cin.ignore();
		displayAdmin();
	}
}

// Removes item from list.(Admin)
void Shop::removeItem()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

	// Asks if remove tools or supplies.
	cout << endl << "What type of wares would you want to remove?" << endl;
	cout << "1) Tool" << endl;
	cout << "2) Supply" << endl;
	cout << "Your choice: ";
	
	getline( cin, currentChoice );

	// Pre: compare input given.
	if( currentChoice == "1" || currentChoice == "Tool" )
	{
		// Post: remove tool.

		// Choose tool.
		int c = 0;

		while( c > tools.size() || c <= 0 )
		{
			cout << "Choose tool to edit: " << endl << endl;
			
			/*list<Tool>::iterator toolIT = tools.begin();

			for( int i = 0; i < tools.size(); i++ )
			{
				Tool tempTool = *toolIT;
				std::ostringstream convert;
				string z;
				convert << i + 1;
				z = convert.str();
				cout << z << ") " << tempTool.getName() << endl;
				toolIT++;
			}*/
			displayTools();

			cout << "Choice: ";

			cin >> c;

			if( c > tools.size() || c <= 0 )
				cout << endl << endl << "Invalid choice!" << endl;
		}

		list<Tool>::iterator toolIT = tools.begin();
		for( int i = 0; i < c - 1; i++ )
		{
			toolIT++;
		}

		tools.erase( toolIT );
		cout << endl << "Item removed!" << endl;
		
		cin.ignore();
		displayAdmin();

	}
	else if( currentChoice == "2" || currentChoice == "Supply" )
	{
		// Post: remove supply.
		
		// Choose tool.
		int c = 0;

		while( c > tools.size() || c <= 0 )
		{
			cout << "Choose tool to edit: " << endl << endl;
			
			/*list<Tool>::iterator toolIT = tools.begin();

			for( int i = 0; i < tools.size(); i++ )
			{
				Tool tempTool = *toolIT;
				std::ostringstream convert;
				string z;
				convert << i + 1;
				z = convert.str();
				cout << z << ") " << tempTool.getName() << endl;
				toolIT++;
			}*/
			displaySupplies();

			cout << "Choice: ";

			cin >> c;

			if( c > supplies.size() || c <= 0 )
				cout << endl << endl << "Invalid choice!" << endl;
		}

		list<Supply>::iterator supIT = supplies.begin();
		for( int i = 0; i < c - 1; i++ )
		{
			supIT++;
		}

		supplies.erase( supIT );
		cout << endl << "Item removed!" << endl;
		
		cin.ignore();
		displayAdmin();
	}
}

// Saves list of items.(Admin)
void Shop::saveUpdate()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

	outputS.open( "tools.txt" );

	// Declare required var.
	list<Tool>::iterator toolIT = tools.begin();
	Tool tempTool;
	string name, desc, fact, col;
	double cost;
	int rate;

	// Saves data of every tools.
	for( int i = 0; i < tools.size(); i ++ )
	{
		tempTool = *toolIT;

		name = tempTool.getName();
		desc = tempTool.getDesc();
		cost = tempTool.getCost();
		fact = tempTool.getFact();
		col = tempTool.getColor();
		rate = tempTool.getRating();

		outputS << name << '#' << desc << '#' << cost << '#' << fact << '#' << col << '#' << rate << '#';

		toolIT++;
	}

	outputS << '#';

	outputS.close();

	outputS.open( "supplies.txt" );

	// Declare required var.
	list<Supply>::iterator supIT = supplies.begin();
	Supply tempSup;
	//string name, desc, fact;
	//double cost;
	int amt;

	// Saves data of every supply.
	for( int i = 0; i < supplies.size(); i ++ )
	{
		tempSup = *supIT;

		name = tempSup.getName();
		desc = tempSup.getDesc();
		cost = tempSup.getCost();
		fact = tempSup.getFact();
		amt = tempSup.getAmt();

		outputS << name << '#' << desc << '#' << cost << '#' << fact << '#' << amt << '#';

		supIT++;
	}

	outputS << '#';

	outputS.close();

	cout << endl << "Update saved!" << endl;
	displayAdmin();
}

// Displays shopper's UI.
void Shop::displayShopper()
{
	cout << "Welcome to the wares shop! Which section would you like to access?" << endl << endl;

	// Print choices.
	for( int i = 0; i < 2; i++ )
	{
		switch( i )
		{
		case 0: cout << "1) Tools" << endl;
			break;
		case 1: cout << "2) Supplies" << endl;
			break;
		default: cout << "" << endl;
			break;
		}
	}

	cout << "Your choice: ";
	getline( cin, currentChoice );

	// Fire methods for each choice.
	if( currentChoice == "1" || currentChoice == "Tools" )
	{
		// Post: add items.
		showShopperOptions( 0 );
	}
	else if( currentChoice == "2" || currentChoice == "Supplies" )
	{
		// Post: edit items.
		showShopperOptions( 1 );
	}
	else
	{
		// Post: re-prompt.
		cout << "Please enter a valid choice! " << endl << endl;
		displayShopper();
	}
}
	
// Shows items on sale and options.
void Shop::showShopperOptions( const int &type )
{
	// Pre: check whether user picks tools or supplies.
	if( type == 0 )
	{
		// Post: display tools and options.
		cout << endl << endl << "Here are all the tools available! " <<
			"To add an item to your cart, enter the item number with the prefix b. " << 
			"To look at the details of the item, enter the item number with the prefix d. " <<
			"You can sort the items using the sort command. " <<
			"In order to go back, type back." << 
			"Finalize your purchase by typing buy." << endl << endl;

		displayTools();

		// Get input.
		getline( cin, currentChoice );

		// Pre: check input.
		if( currentChoice == "buy" )
		{
			// Post: buy.
			cout << endl << endl << "Items bought: " << endl;

			list<Ware>::iterator cartIT = cart.begin();
			double cost = 0;

			for( int i = 0; i < cart.size(); i++ )
			{
				cout << ( *cartIT ).getName() << endl;
				cost += ( *cartIT ).getCost();

				cartIT++;
			}

			cout << endl << endl << "Total cost: $" << std::fixed << cost << endl;
		}
		else if( currentChoice == "back" )
		{
			displayShopper();
		}
		else if( currentChoice.substr( 0, 1 ) == "b" )
		{
			// Post: add to cart.
			stringstream convert( currentChoice.substr( 1 ) );
			int itemNo;

			// Pre: valid input.
			if( convert >> itemNo )
			{
				// Post: main logic.
				if( itemNo >= 0 && itemNo <= tools.size() )
				{
					list<Tool>::iterator toolIT = tools.begin();

					for( int i = 0; i < itemNo - 1; i++ )
					{
						toolIT++;
					}

					cart.push_back( *toolIT );
					cout << "Item added to cart!" << endl << endl;
					showShopperOptions( 0 );
				}
				else
				{
					cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
					string trash;
					cin >> trash;
					cin.ignore();
					showShopperOptions( 0 );
				}
			}
			else
			{
				// Post: re-prompt.
				cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
				string trash;
				cin >> trash;
				showShopperOptions( 0 );
			}
		}
		else if( currentChoice.substr( 0, 1 ) == "d" )
		{
			// Post: show description.
			stringstream convert( currentChoice.substr( 1 ) );
			int itemNo;

			// Pre: valid input.
			if( convert >> itemNo )
			{
				// Post: main logic.
				if( itemNo >= 0 && itemNo <= tools.size() )
				{
					list<Tool>::iterator toolIT = tools.begin();

					for( int i = 0; i < itemNo - 1; i++ )
					{
						toolIT++;
					}

					cout << endl << endl << *toolIT;
					cout << endl << "Enter any key to go back..." << endl;
					string trash;
					cin >> trash;
					cin.ignore();
					showShopperOptions( 0 );
				}
				else
				{
					cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
					string trash;
					cin >> trash;
					cin.ignore();
					showShopperOptions( 0 );
				}
			}
			else
			{
				// Post: re-prompt.
				cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
				string trash;
				cin >> trash;
				showShopperOptions( 0 );
			}
		}
		else if( currentChoice == "sort" )
		{
			// Post: show sort settings.
			cout << endl << "Sort by: " << endl <<
				"1) Name" << endl <<
				"2) Cost" << endl <<
				"3) Rating" << endl;

			getline( cin, currentChoice );

			while( !( currentChoice == "1" || currentChoice == "2" || currentChoice == "3" ) )
			{
				cout << endl << "Invalid input! Please re-enter your choice: " ;
				getline( cin, currentChoice );
			}

			if( currentChoice == "1" )
			{
				tools.sort( cmp_by_name );
				cout << endl << "List sorted!" << endl;
				cin.ignore();
				showShopperOptions( 0 );
			}
			else if( currentChoice == "2" )
			{
				tools.sort( cmp_by_cost );
				cout << endl << "List sorted!" << endl;
				cin.ignore();
				showShopperOptions( 0 );
			}
			else if( currentChoice == "3" )
			{
				tools.sort( cmp_by_rate );
				cout << endl << "List sorted!" << endl;
				cin.ignore();
				showShopperOptions( 0 );
			}
		}
		else
		{
			cout << endl << "Invalid command!" << endl;
			getline( cin, currentChoice );
		}
	}
	else if( type == 1 )
	{
		// Post: display supplies and options.
		cout << endl << endl << "Here are all the supplies available! " <<
			"To add an item to your cart, enter the item number with the prefix b. " << 
			"To look at the details of the item, enter the item number with the prefix d. " <<
			"You can sort the items using the sort command. " <<
			"In order to go back, type back." << 
			"Finalize your purchase by typing buy." << endl << endl;

		displaySupplies();

		// Get input.
		getline( cin, currentChoice );

		// Pre: check input.
		if( currentChoice == "buy" )
		{
			// Post: buy.
			cout << endl << endl << "Items bought: " << endl;

			list<Ware>::iterator cartIT = cart.begin();
			double cost = 0;

			for( int i = 0; i < cart.size(); i++ )
			{
				cout << ( *cartIT ).getName() << endl;
				cost += ( *cartIT ).getCost();

				cartIT++;
			}

			cout << endl << endl << "Total cost: $" << std::fixed << cost << endl;
		}
		else if( currentChoice == "back" )
		{
			displayShopper();
		}
		else if( currentChoice.substr( 0, 1 ) == "b" )
		{
			// Post: add to cart.
			stringstream convert( currentChoice.substr( 1 ) );
			int itemNo;

			// Pre: valid input.
			if( convert >> itemNo )
			{
				// Post: main logic.
				if( itemNo >= 0 && itemNo <= supplies.size() )
				{
					list<Supply>::iterator supIT = supplies.begin();

					for( int i = 0; i < itemNo - 1; i++ )
					{
						supIT++;
					}

					cart.push_back( *supIT );
					cout << "Item added to cart!" << endl << endl;
					showShopperOptions( 1 );
				}
				else
				{
					cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
					string trash;
					cin >> trash;
					cin.ignore();
					showShopperOptions( 1 );
				}
			}
			else
			{
				// Post: re-prompt.
				cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
				string trash;
				cin >> trash;
				showShopperOptions( 1 );
			}
		}
		else if( currentChoice.substr( 0, 1 ) == "d" )
		{
			// Post: show description.
			stringstream convert( currentChoice.substr( 1 ) );
			int itemNo;

			// Pre: valid input.
			if( convert >> itemNo )
			{
				// Post: main logic.
				if( itemNo >= 0 && itemNo <= supplies.size() )
				{
					list<Supply>::iterator supIT = supplies.begin();

					for( int i = 0; i < itemNo - 1; i++ )
					{
						supIT++;
					}

					cout << endl << endl << *supIT;
					cout << endl << "Enter any key to go back..." << endl;
					string trash;
					cin >> trash;
					cin.ignore();
					showShopperOptions( 1 );
				}
				else
				{
					cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
					string trash;
					cin >> trash;
					cin.ignore();
					showShopperOptions( 1 );
				}
			}
			else
			{
				// Post: re-prompt.
				cout << endl << "Invalid command!" << endl << "Enter any key to go back..." << endl;
				string trash;
				cin >> trash;
				showShopperOptions( 1 );
			}
		}
		else if( currentChoice == "sort" )
		{
			// Post: show sort settings.
			cout << endl << "Sort by: " << endl <<
				"1) Name" << endl <<
				"2) Cost" << endl ;

			getline( cin, currentChoice );

			while( !( currentChoice == "1" || currentChoice == "2" ) )
			{
				cout << endl << "Invalid input! Please re-enter your choice: " ;
				getline( cin, currentChoice );
			}

			if( currentChoice == "1" )
			{
				supplies.sort( cmp_by_name );
				cout << endl << "List sorted!" << endl;
				cin.ignore();
				showShopperOptions( 1 );
			}
			else if( currentChoice == "2" )
			{
				supplies.sort( cmp_by_cost );
				cout << endl << "List sorted!" << endl;
				cin.ignore();
				showShopperOptions( 1 );
			}
		}
		else
		{
			cout << endl << "Invalid command!" << endl;
			getline( cin, currentChoice );
		}

	}
}

// Display tools from list.
void Shop::displayTools()
{
	list<Tool>::iterator toolIT = tools.begin();

	for( int i = 0; i < tools.size(); i++ )
	{
		stringstream n;
		string no;
		n << i + 1;
		no = n.str() + ") ";
		cout << no << ( *toolIT ).getName() << endl;

		toolIT++;
	}
}

// Display supplies from list.
void Shop::displaySupplies()
{
	list<Supply>::iterator supIT = supplies.begin();

	for( int i = 0; i < supplies.size(); i++ )
	{
		stringstream n;
		string no;
		n << i + 1;
		no = n.str() + ") ";
		cout << no << ( *supIT ).getName() << endl;

		supIT++;
	}
}