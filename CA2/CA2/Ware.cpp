#include <iostream>
#include <string>
#include "Ware.h"

using namespace std;

// Constructor method.
Ware::Ware( string n, string desc, double c, string fact )
{
	name = n;
	description = desc;
	cost = c;
	factory = fact;

} // End of constructor method.

// Overloading operator<< to print ware details.
std::ostream& operator<<( std::ostream& os, const Ware& ware )
{
	os << "Name: " << ware.getName() << endl
		<< ware.getDesc() << endl
		<< "Cost: " << ware.getCost() << endl
		<< "Factory: " << ware.getFact() << endl;

	return os;

}// End of overloading operator<<.

// Returns name of ware.
string Ware::getName() const 
{
	return name;
}

// Returns description of ware.
string Ware::getDesc() const
{
	return description;
}

// Returns cost of ware.
double Ware::getCost() const
{
	return cost;
}

// Returns factory where ware is produced.
string Ware::getFact() const
{
	return factory;
}

// Sets name of ware.
void Ware::setName( string &n )
{
	name = n;
}

// Sets description of ware.
void Ware::setDesc( string &desc )
{
	description = desc;
}

// Sets cost of ware.
void Ware::setCost( double &c )
{
	cost = c;
}

// Sets factory of ware.
void Ware::setFact( string &fact )
{
	factory = fact;
}
