#include <iostream>
#include <string>
#include "Tool.h"

using namespace std;

// Constructor method.
Tool::Tool( string n, string desc, double c, string fact, string col, int rate ) : Ware( n, desc, c, fact )
{
	color = col;
	rating = rate;

} // End of constructor method.

// Overloading operator<< to print ware details.
std::ostream& operator<<( std::ostream& os, const Tool& tool )
{
	os << "Name: " << tool.getName() << endl
		<< "Color: " << tool.getColor() << endl
		<< tool.getDesc() << endl
		<< "Cost: " << tool.getCost() << endl
		<< "Factory: " << tool.getFact() << endl
		<< "Rating: " << tool.getRating() << endl;

	return os;

}// End of overloading operator<<.

// Returns color of tool.
string Tool::getColor() const
{
	return color;
}

// Returns rating of tool.
int Tool::getRating() const
{
	return rating;
}

// Sets color of tool.
void Tool::setColor( string &col )
{
	color = col;
}

// Sets rating of tool.
void Tool::setRating( int &rate )
{
	rating = rate;
}