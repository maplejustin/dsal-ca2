#pragma once
#include <string>

using namespace std;

class Ware
{

private:
	string name; // Name of ware.
	string description; // Description of ware.
	double cost; // Cost of ware.
	string factory; // Factory where ware is produced.

public:
	Ware( string n = "", string desc = "", double c = 0, string fact = "" ); // Constructor method.
	string getName() const; // Returns name of ware.
	string getDesc() const; // Returns description of ware.
	double getCost() const; // Returns cost of ware.
	string getFact() const; // Returns factory where ware is produced.
	void setName( string &n ); // Sets name of ware.
	void setDesc( string &desc ); // Sets description of ware.
	void setCost( double &c ); // Sets cost of ware.
	void setFact( string &fact ); // Sets factory of ware.
	friend ostream& operator<<( ostream& os, const Ware& ware ); // Overloading operator to print ware details.

};