#include <iostream>
#include <string>
#include "Supply.h"

using namespace std;

// Constructor method.
Supply::Supply( string n, string desc, double c, string fact, int amt ) : Ware( n, desc, c, fact )
{
	amount = amt;

} // End of constructor method.

// Overloading operator<< to print ware details.
std::ostream& operator<<( std::ostream& os, const Supply& supply )
{
	os << "Name: " << supply.getName() << endl
		<< supply.getDesc() << endl
		<< "Cost: " << supply.getCost() << endl
		<< "Factory: " << supply.getFact() << endl
		<< "Amount: " << supply.getAmt() << endl;

	return os;

}// End of overloading operator<<.

// Returns amount of tool.
int Supply::getAmt() const
{
	return amount;
}

// Sets rating of tool.
void Supply::setAmt( int &amt )
{
	amount = amt;
}