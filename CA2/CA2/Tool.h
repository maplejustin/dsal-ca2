#pragma once
#include <string>
#include "Ware.h"

class Tool : public Ware
{

private:
	string color; // Color of tool.
	int rating; // Rating of tool.

public:
	Tool( string n = "", string desc = "", double c = 0, string fact = "", 
		string col = "", int rate = 0 ); // Constructor method.
	string getColor() const; // Returns color of tool.
	int getRating() const; // Returns rating of tool.
	void setColor( string &col ); // Sets color of tool.
	void setRating( int &rate ); // Sets rating of tool.
	friend ostream& operator<<( ostream& os, const Tool& tool ); // Overloading operator to print ware details.
};