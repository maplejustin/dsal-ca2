#pragma once
#include <string>
#include "Ware.h"

class Supply : public Ware
{

private:
	int amount; // Amount of supply per unit.

public:
	Supply( string n = "", string desc = "", double c = 0, string fact = "", 
		int amt = 0 ); // Constructor method.
	int getAmt() const; // Returns amount of supply per unit.
	void setAmt( int &amt ); // Sets amount of supply per unit.
	friend ostream& operator<<( ostream& os, const Supply& supply ); // Overloading operator to print ware details.
};