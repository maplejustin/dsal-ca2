#include <iostream>
#include <fstream>
#include <string>
#include "Ware.h"
#include "Tool.h"
#include "Shop.h"

using namespace std;

void main()
{
	/*Ware nails = Ware( 1, "Nails", "Pointy!", 4.50, "ABC Factory" );
	Ware hammer = Tool( 2, "Hammer", "Dayum!", 10.00, "ABC Factory", "Mylar", 6 );
	Ware *aTool = new Tool( 3, "aTool", "lol", 5.00, "ABC Factory", "Red", 5 );
	//cout << nails;
	cout << endl;
	//cout << hammer;
	Tool *temp = static_cast<Tool*>(aTool);
	cout << *temp;*/

	// Bool to indicate if admin/shopper mode.
	bool isAdmin = false;

	// Prompt user to tell whether hes an admin.
	cout << "Welcome to our Wares!" << endl << endl
		<< "Are you our admin? If so please enter the 6 digit PIN number, else key in anything." << endl << endl;

	string pin = "";

	// Sets admin/shopper mode.
	cout << "PIN: ";
	getline( cin, pin );
	if( pin == "123456" )
	{
		isAdmin = true;
	}
	else
	{
		isAdmin = false;
	}

	// Sets up shop.
	Shop shop = Shop( isAdmin );
	shop.display();

	/*
	// Test file input/output
	ifstream inputS;
	ofstream outputS;

	inputS.open( "test.txt" );
	outputS.open( "test.txt", ofstream::app );

	int testInt = 1;
	while( testInt != 0 )
	{
		cout << "Enter a number: ";
		cin >> testInt;

		outputS << testInt;
		outputS << " ";
	}

	outputS << 0;
	outputS << " ";
	outputS.close();

	int outInt = 1;
	string blank = "";
	
	while( outInt != 0 )
	{
		inputS >> outInt;
		cout << outInt << " ";
		inputS >> blank;
	}

	inputS.close();
	*/
}